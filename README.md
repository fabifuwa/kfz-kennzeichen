# KFZ-Kennzeichen <img src="fastlane/metadata/android/en-US/images/icon.png" height="50">

Eine einfache offline Android App zum abfragen von KFZ-Kennzeichen in Deutschland.  
Datenbasis: https://github.com/offene-daten/kennzeichen

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/luke.kfz/)

  ## Screenshot
  
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/01.jpg">  
